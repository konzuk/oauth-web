import { BaseViewComponent } from './BaseViewComponent';

import { OnDestroy, OnInit, Component } from '@angular/core';
import { StarterService } from '../../services/starter/starter.service';
import { MyEventService } from 'src/app/services/my-event.service';

@Component({
  selector: 'app-starter',
  templateUrl: 'starter.template.html'
})
export class StarterComponent extends BaseViewComponent implements OnDestroy, OnInit {


  public nav: any;
  private _key: String;
  content: String;



  public constructor(private ser: StarterService, myEvent: MyEventService) {
    super(myEvent);
    this.nav = document.querySelector('nav.navbar');
  }

  key(): String { return this._key; }


  public ngOnInit(): any {
    this.nav.className += ' white-bg';
    this._key = this.ser.getData();

  }

  public ngOnReInit() {

    this._key = this.ser.getData();
  }



  public ngOnDestroy(): any {
    this.nav.classList.remove('white-bg');
  }

  handleData(data) {
    this.content = data.message;
  }

}
