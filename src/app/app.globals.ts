import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class Globals {
    currentUrl: String = '';
    previousUrl: String = null;
    loginUrl: String = '/login';
    isRefreshToken = false;
    currentServiceKey: String = '';
    refreshTokenOnError = false;
    requestGoogleLogin = false;
}
