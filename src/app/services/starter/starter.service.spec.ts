import { TestBed, inject } from '@angular/core/testing';

import { StarterService } from './starter.service';

describe('StarterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StarterService]
    });
  });

  it('should be created', inject([StarterService], (service: StarterService) => {
    expect(service).toBeTruthy();
  }));
});
