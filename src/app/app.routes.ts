import {Routes} from '@angular/router';


import {StarterComponent} from './views/appviews/starter.component';
import {LoginComponent} from './views/appviews/login.component';

import {BlankLayoutComponent} from './components/common/layouts/blankLayout.component';
import {BasicLayoutComponent} from './components/common/layouts/basicLayout.component';

export const ROUTES: Routes = [
  // Main redirect
  {path: '', redirectTo: 'starter', pathMatch: 'full'},

  // App views
  {
    path: '', component: BasicLayoutComponent,
    children: [
      {path: 'starter', component: StarterComponent}
    ]
  },
  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
    ]
  },

  // Handle all other routes
  {path: '**',  redirectTo: 'starter'}
];




  // {
  //   path: 'dashboards', component: BasicLayoutComponent,
  //   children: [
  //     {path: 'dashboard1', component: Dashboard1Component},
  //     {path: 'dashboard2', component: Dashboard2Component},
  //     {path: 'dashboard3', component: Dashboard3Component},
  //     {path: 'dashboard4', component: Dashboard4Component},
  //     {path: 'dashboard5', component: Dashboard5Component}
  //   ]
  // },
  // {
  //   path: 'dashboards', component: TopNavigationLayoutComponent,
  //   children: [
  //     {path: 'dashboard41', component: Dashboard41Component}
  //   ]
  // },
