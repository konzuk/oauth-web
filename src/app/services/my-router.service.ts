import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MyEventService } from './my-event.service';

@Injectable()
export class MyRouterService {
  constructor(
    private router: Router,
    private myEvent: MyEventService
  ) {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.myEvent.RouterNavigateEndEvent.next(event.url);
        }
      });
  }
}
