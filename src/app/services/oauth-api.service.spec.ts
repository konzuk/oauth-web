import { TestBed, inject } from '@angular/core/testing';

import { OauthApiService } from './oauth-api.service';

describe('OauthApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OauthApiService]
    });
  });

  it('should be created', inject([OauthApiService], (service: OauthApiService) => {
    expect(service).toBeTruthy();
  }));
});
