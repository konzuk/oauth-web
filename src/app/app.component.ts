import { Component, OnDestroy, OnInit } from '@angular/core';
import { MyEventService } from './services/my-event.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Globals } from 'src/app/app.globals';
import { OauthApiService } from './services/oauth-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  constructor(
    private myEvent: MyEventService,
    private spinner: NgxSpinnerService,
    private oauthApi: OauthApiService,
    private globalVar: Globals
  ) { }

  ngOnInit(): void {

    this.myEvent.ToogleSpinnerEvent.subscribe(show => {
      if (show) {
        this.spinner.show();
      } else {
        this.spinner.hide();
      }
    });

    this.myEvent.RouterNavigateEndEvent.subscribe(url => {
      this.globalVar.previousUrl = this.globalVar.currentUrl;
      this.globalVar.currentUrl = url;
    });

    this.myEvent.CallRefreshTokenEvent.subscribe(() => {
      this.oauthApi.refreshTokenOauth();
    });


  }
  ngOnDestroy(): void {

    this.myEvent.ToogleSpinnerEvent.unsubscribe();
    this.myEvent.RouterNavigateEndEvent.unsubscribe();
  }


}

