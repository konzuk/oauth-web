import { Component, OnInit } from '@angular/core';
import { detectBody, correctHeight } from '../../../app.helpers';
import { HostListener } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'app-basic',
  templateUrl: 'basicLayout.template.html'
})
export class BasicLayoutComponent implements OnInit {

  public ngOnInit(): any {

    jQuery('body').addClass('fixed-sidebar');
    jQuery('body').addClass('fixed-nav');
    jQuery('body').addClass('fixed-nav-basic');

    detectBody();
    correctHeight();
  }



  @HostListener('window:resize', ['$event'])
  public onResize() {
    detectBody();
    correctHeight();
  }

}
