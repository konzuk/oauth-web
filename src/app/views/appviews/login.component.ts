import { Component, OnInit, AfterViewInit } from '@angular/core';
import { OauthApiService } from '../../services/oauth-api.service';
import { AuthService, SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { Globals } from '../../app.globals';

@Component({
  selector: 'app-login',
  templateUrl: 'login.template.html'
})
export class LoginComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {
    this.oauthApi.logOutOauth();
  }

  ngOnInit(): void {

    this.authService.authState.subscribe((user) => {
      if (user && user instanceof SocialUser && user.id && this.globalVar.requestGoogleLogin) {
        this.signInGoogle(user);
        this.authService.signOut();
        this.globalVar.requestGoogleLogin = false;
      }
    });
  }

  constructor(private oauthApi: OauthApiService, private authService: AuthService, private globalVar: Globals) { }

  signIn(username, password, event): void {
    event.preventDefault();
    this.oauthApi.passwordOauth(username, password);
  }

  signInGoogle(user: SocialUser): void {
    event.preventDefault();
    this.oauthApi.thirdPartyOauth(user);
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.globalVar.requestGoogleLogin = true;
  }

}
