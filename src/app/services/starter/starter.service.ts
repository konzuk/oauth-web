import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { RxCacheService } from 'ngx-rxcache';
import { MyEventService } from '../my-event.service';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../app.globals';

@Injectable({
  providedIn: 'root'
})
export class StarterService extends ApiService {


  serviceKey(): String { return 'starter'; }

  constructor(http: HttpClient, cache: RxCacheService, myEvent: MyEventService, globalVar: Globals) {
    super(http, cache, myEvent, globalVar);
  }

  public getData(): String {
    const headers = this.secureHeader();
    const res = this.getService('starter', headers);
    res.subscribe();
    return this.serviceKey();
  }
}
